
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String workDate;
	String jig;
	String WC;
	String GRNM;
	String targetDateTime;
	int inCycleTime;
	int waitTime;
	int alarmTime;
	int noConnectionTime;
	
	int tgCyl;
	int cntCyl;
	String line;
	String sDate;
	String eDate;
	String date;
	String tgDate;
	//common
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String msg;
	String rgb;
	String name;
	
}
